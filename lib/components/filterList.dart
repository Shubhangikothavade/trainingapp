import 'package:flutter/material.dart';
import 'package:training_app/constants/textstyleconstant.dart';

class FilterList extends StatefulWidget {
  final filterlabel;

  FilterList({this.filterlabel});

  @override
  _FilterListState createState() => _FilterListState();
}

class _FilterListState extends State<FilterList> {
  @override
  Widget build(BuildContext context) {
    return  Container(
        padding: EdgeInsets.all(10),
        //color: Colors.grey,
        child: Text('${widget.filterlabel}', style: filterlabel));
  }
}
