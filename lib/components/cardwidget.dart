import 'package:flutter/material.dart';
import 'package:training_app/constants/textstyleconstant.dart';

class CardWidget extends StatefulWidget {
  final date;
  final address;
  final trainername;

  CardWidget({
    this.date,this.trainername,this.address
});

  @override
  _CardWidgetState createState() => _CardWidgetState();
}

class _CardWidgetState extends State<CardWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      child: Card(
          child: IntrinsicHeight(
            child: Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListTile(
                        title: Text('${widget.date}',style: filterlabel.copyWith(fontSize: 15),),
                        subtitle: Text('2019',style: filterlabel.copyWith(fontSize: 15),),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: Text('08:30am-12:30pm'),
                      ),
                       SizedBox(height: 10,),
                       Padding(
                         padding: const EdgeInsets.only(left: 10.0,bottom: 10),
                         child: Text('${widget.address}',
                         maxLines: 3,
                         overflow: TextOverflow.ellipsis,),
                       ),

                    ],
                  ),
                ),
                VerticalDivider(
                  color: Colors.grey,
                  thickness: 1,
                  width: 20,
                ),
                Expanded(
                  flex: 2,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListTile(
                        title: Text('Filling Fast!',style: TextStyle(color: Colors.red),),
                        subtitle: Text('Safe Scrum Master (4.6)',style: filterlabel,),
                      ),
                      ListTile(
                        leading: CircleAvatar(
                          radius: 20,
                          child: Icon(Icons.account_circle_rounded),
                        ),
                        title: Text('${widget.trainername}'),
                        subtitle: Text('Helen Gribbie'),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
