import 'package:flutter/material.dart';
import 'package:training_app/screens/homepage.dart';

void main() {
  runApp(MaterialApp(
    title: 'Trainings App',
    home: HomePage(),
    debugShowCheckedModeBanner: false,
  ));
}
