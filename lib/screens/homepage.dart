import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:training_app/components/filterList.dart';
import 'package:training_app/constants/textstyleconstant.dart';
import 'package:training_app/model/notification_setting.dart';

import '../components/cardwidget.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool? chbWest = false;
  bool? chbChicago = false;
  bool? chbPhoenix = false;
  bool? chbDallas = false;
  bool? chbSanDiego = false;
  bool? chbSanFransisco = false;
  bool? chbNewYork = false;

  final notifications = [
    NotificationSetting(title: 'West Des Moines'),
    NotificationSetting(title: 'Chicago, IL'),
    NotificationSetting(title: 'Phoenix, AZ'),
    NotificationSetting(title: 'Dallas, TX'),
    NotificationSetting(title: 'San Diego, CA'),
    NotificationSetting(title: 'San Fransisco, CA'),
    NotificationSetting(title: 'New York, ZK'),
  ];

  final _controller = CarouselController();
  final assetimage = [
    Image.asset(
      'asset/training1.jpg',
      fit: BoxFit.cover,
    ),
    Image.asset(
      'asset/training2.jpg',
      fit: BoxFit.cover,
    ),
    Image.asset(
      'asset/training3.jpg',
      fit: BoxFit.cover,
    ),
    Image.asset(
      'asset/training4.jpg',
      fit: BoxFit.cover,
    ),
  ];

  @override
  void dispose() {
    // TODO: implement dispose
    //_controller.dispose();
    super.dispose();
  }

  final trainingcost = [850, 225, 500, 730];
  int carouselindex = 0;

  void showBottomSheet() => showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, setState) {
            return ListView(
              children: [
                filter_label(),
                Divider(
                  thickness: 2,
                ),
                IntrinsicHeight(
                  child: Row(
                    //crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          FilterList(
                            filterlabel: 'Sort By',
                          ),
                          FilterList(
                            filterlabel: 'Location',
                          ),
                          FilterList(
                            filterlabel: 'Training Name',
                          ),
                          FilterList(
                            filterlabel: 'Trainer',
                          ),
                        ],
                      ),
                      VerticalDivider(
                        color: Colors.black,
                        thickness: 2,
                      ),
                      Row(
                        children: [
                          Column(
                            //mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              /*CheckboxListTile(
                                onChanged: (value){
                                  setState(){
                                    chbSanFransisco = value;
                                  }
                                },
                                value: chbSanFransisco,
                                title: Text('SanFransisco'),
                              ),*/
                              Row(
                                children: [
                                  Checkbox(
                                    value: chbWest,
                                    onChanged: (val) {
                                      setState(() {
                                        chbWest = val;
                                      });
                                    },
                                  ),
                                  Text('West Des Moines',
                                      style: filterlabel),
                                ],
                              ),
                              Row(
                                children: [
                                  Checkbox(
                                    value: chbChicago,
                                    onChanged: (val) {
                                      setState(() {
                                        chbChicago = val;
                                      });
                                    },
                                  ),
                                  Text('Chicago', style: filterlabel),
                                ],
                              ),
                              Row(
                                children: [
                                  Checkbox(
                                    value: chbPhoenix,
                                    onChanged: (val) {
                                      setState(() {
                                        chbPhoenix = val;
                                      });
                                    },
                                  ),
                                  Text('Phoenix, AZ', style: filterlabel),
                                ],
                              ),
                              Row(
                                children: [
                                  Checkbox(
                                    value: chbSanDiego,
                                    onChanged: (val) {
                                      setState(() {
                                        chbSanDiego = val;
                                      });
                                    },
                                  ),
                                  Text('San Diego, CA', style: filterlabel),
                                ],
                              ),
                              Row(
                                children: [
                                  Checkbox(
                                    value: chbSanFransisco,
                                    onChanged: (val) {
                                      setState(() {
                                        chbSanFransisco = val;
                                      });
                                    },
                                  ),
                                  Text('San Fransisco, CA',
                                      style: filterlabel),
                                ],
                              ),
                              Row(
                                children: [
                                  Checkbox(
                                    value: chbNewYork,
                                    onChanged: (val) {
                                      setState(() {
                                        chbNewYork = val;
                                      });
                                    },
                                  ),
                                  Text('New York, ZK', style: filterlabel),
                                ],
                              ),

                              //...notifications.map(buildSingleCheckbox).toList(),
                            ],
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            );
          });
        },
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  height: 330,
                  color: Colors.white,
                ),
                Container(
                  height: 200,
                  color: Colors.red,
                ),
                Positioned(
                    child: Container(
                  padding: EdgeInsets.all(20),
                  height: 140,
                  color: Colors.transparent,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [headingrow(), highlightrow()],
                  ),
                )),
                arrow_back(controller: _controller),
                arrow_forword(controller: _controller),
                Positioned(
                    left: 120,
                    top: 130,
                    //right: 20,
                    child: Stack(
                      children: [
                        Container(
                          //height: 150,
                          width: 200,
                          //margin: EdgeInsets.only(left: 20,right: 20),
                          color: Colors.transparent,
                          child: CarouselSlider.builder(
                            carouselController: _controller,
                            itemCount: assetimage.length,
                            itemBuilder: (context, index, realIndex) {
                              carouselindex = index;
                              final imageurl = assetimage[index];
                              return imageurl;
                            },
                            options: CarouselOptions(
                              height: 150,
                              //autoPlayAnimationDuration: Duration(seconds: 2),
                              //autoPlay: true,
                              viewportFraction: 1,
                            ),
                          ),
                        ),
                        Positioned(
                          left: 0,
                          bottom: 0,
                          child: Container(
                            height: 20,
                            width: 50,
                            color: Colors.grey,
                            child: Text(
                              'Rs825',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                        Positioned(
                          right: 0,
                          bottom: 0,
                          child: Container(
                            height: 20,
                            width: 100,
                            color: Colors.grey,
                            child: Text(
                              'View Detail ->',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ],
                    )),
                Positioned(
                    left: 20,
                    bottom: 10,
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          showBottomSheet();
                        });
                      },
                      child: Container(
                        height: 30,
                        width: 80,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.transparent,
                          border: Border.all(
                            color: Colors.black,
                            width: 2,
                          ),
                        ),
                        child: Row(
                          children: [
                            Icon(Icons.filter_alt_outlined),
                            Text(
                              'Filter',
                              style: TextStyle(color: Colors.grey[900]),
                            )
                          ],
                        ),
                      ),
                    ))
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    CardWidget(
                      date: 'Oct 11 -13,',
                      address: 'Convention Hall1, Greater Dec Monies',
                      trainername: 'Keynote Speaker1,',
                    ),
                    CardWidget(
                      date: 'Jan 20 -21,',
                      address: 'Convention Hall2, Greater Dec Monies',
                      trainername: 'Keynote Speaker2,',
                    ),
                    CardWidget(
                      date: 'Nov 16 -18,',
                      address: 'Convention Hall3, Greater Dec Monies',
                      trainername: 'Keynote Speaker3,',
                    ),
                  ],
                ),
              ),

              //color: Colors.red,
            )
          ],
        ),
      ),
    );
  }

  Widget buildSingleCheckbox(NotificationSetting notification) {
    return buildCheckBox(
      notification: notification,
      onClicked: () {
        setState(() {
          final newValue = !notification.value;
          notification.value = newValue;
        });
      },
    );
  }

  Widget buildCheckBox({
    required NotificationSetting notification,
    required VoidCallback onClicked,
  }) {
    return ListTile(
      onTap: onClicked,
      leading: Checkbox(
        value: notification.value,
        onChanged: (value) => onClicked(),
      ),
      title: Text(
        notification.title,
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.normal),
      ),
    );
  }
}

class filter_label extends StatelessWidget {
  const filter_label({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          'Sort and Filters',
          style: filterlabel,
        ),
        IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.close))
      ],
    );
  }
}

class arrow_forword extends StatelessWidget {
  const arrow_forword({
    Key? key,
    required CarouselController controller,
  })  : _controller = controller,
        super(key: key);

  final CarouselController _controller;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 170,
      right: 0,
      child: InkWell(
        onTap: () {
          _controller.nextPage(duration: Duration(microseconds: 500));
        },
        child: Container(
            color: Colors.grey,
            height: 60,
            child: Icon(
              Icons.arrow_forward_ios,
              color: Colors.white,
            )),
      ),
    );
  }
}

class arrow_back extends StatelessWidget {
  const arrow_back({
    Key? key,
    required CarouselController controller,
  })  : _controller = controller,
        super(key: key);

  final CarouselController _controller;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 170,
      child: InkWell(
        onTap: () {
          _controller.previousPage(duration: Duration(microseconds: 500));
        },
        child: Container(
            color: Colors.grey,
            height: 60,
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            )),
      ),
    );
  }
}

class highlightrow extends StatelessWidget {
  const highlightrow({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          'Highlights',
          style: TextStyle(color: Colors.white, fontSize: 20),
        )
      ],
    );
  }
}

class headingrow extends StatelessWidget {
  const headingrow({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Trainings',
          style: TextStyle(
              fontSize: 30,
              color: Colors.white,
              fontWeight: FontWeight.bold,
              letterSpacing: 2),
        ),
        Icon(
          Icons.menu,
          color: Colors.white,
        )
      ],
    );
  }
}
